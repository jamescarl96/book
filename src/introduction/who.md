# Who develops Veloren?

Veloren is developed by a community of volunteers that have one unifying aim: creating a bright,
colourful, fun world that can be explored and interacted with in unique and interesting ways.

Below, you can find community information.

## Discord

The Veloren community (both developers and players) is most active on the Veloren Discord server.
You can join the server using the link below:

<https://discord.gg/BvQuGze>

## Website

The Veloren website contains weekly blog updates, developer information and download links.
You can find it at [www.veloren.net](https://www.veloren.net)

The website also hosts a developer manual (you're probably reading it now!) at [book.veloren.net](https://book.veloren.net)

## Reddit

Veloren has its own subreddit. You can find it at [reddit.com/r/veloren](https://www.reddit.com/r/veloren)

## Twitter

Veloren has a Twitter page at [twitter.com/velorenproject](https://twitter.com/velorenproject).

## Wiki

Veloren has a wiki site. It is accessible at [wiki.veloren.net](https://wiki.veloren.net)
